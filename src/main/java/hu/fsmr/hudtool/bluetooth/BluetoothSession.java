package hu.fsmr.hudtool.bluetooth;

import javax.bluetooth.RemoteDevice;
import javax.microedition.io.Connector;
import javax.microedition.io.StreamConnection;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by blase on 2018.02.28..
 */
public class BluetoothSession {
    private RemoteDevice remoteDevice;
    private StreamConnection streamConnection;
    private OutputStream os;
    private InputStream is;

    public BluetoothSession(RemoteDevice remoteDevice) throws IOException {
        streamConnection = (StreamConnection) Connector.open(remoteDevice.getBluetoothAddress());
        os = streamConnection.openOutputStream();
        is = streamConnection.openInputStream();
    }

    public BluetoothSession(String connString) throws IOException {
        streamConnection = (StreamConnection) Connector.open(connString);
        os = streamConnection.openOutputStream();
        is = streamConnection.openInputStream();
    }

    public void sendMessage(String message){
        try{
            synchronized (streamConnection){

                os.write(message.getBytes());
            }
        }catch (Exception e){

        }
    }

    public void readMessage(){

    }

    public void isOpen(){

    }

    public void close(){

        try {
            streamConnection.close();
            os.close();
            is.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
