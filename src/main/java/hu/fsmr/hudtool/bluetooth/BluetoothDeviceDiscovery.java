package hu.fsmr.hudtool.bluetooth;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import javax.bluetooth.*;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by blase on 2018.02.28..
 */
public class BluetoothDeviceDiscovery implements DiscoveryListener {

    // object used for waiting
    private static Object lock = new Object();

    // vector containing the devices discovered
    private static ArrayList<RemoteDevice> devices;

    public BluetoothDeviceDiscovery() {
        devices = new ArrayList<RemoteDevice>();
    }

    public ObservableList<RemoteDevice> getDevices() {
        ObservableList<RemoteDevice> availableConnections = FXCollections.observableArrayList();
        BluetoothDeviceDiscovery bluetoothDeviceDiscovery = new BluetoothDeviceDiscovery();

        try {
            LocalDevice localDevice = null;
            localDevice = LocalDevice.getLocalDevice();
            DiscoveryAgent agent = localDevice.getDiscoveryAgent();
            System.out.println("Starting device inquiry…");
            agent.startInquiry(DiscoveryAgent.GIAC, bluetoothDeviceDiscovery);
            synchronized(lock){
                lock.wait();
            }
            System.out.println("Device Inquiry Completed. ");
            int deviceCount=devices.size();
            System.out.println("device_count:" +deviceCount);
            if(deviceCount <= 0){
                return null;
            }
            else{
                System.out.println("Bluetooth Devices:" );
                for (int i = 0; i < deviceCount; i++) {
                    RemoteDevice remoteDevice=(RemoteDevice)devices.get(i);
                    availableConnections.add(remoteDevice);
                }
            }
        } catch (BluetoothStateException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return availableConnections;
    }

    public void deviceDiscovered(RemoteDevice btDevice, DeviceClass cod) {
        System.out.println("Device discovered: "+btDevice.getBluetoothAddress());
        if(!devices.contains(btDevice)) {
            devices.add(btDevice);
        }
    }

    public void servicesDiscovered(int transID, ServiceRecord[] servRecord) {

    }

    public void serviceSearchCompleted(int transID, int respCode) {

    }

    public void inquiryCompleted(int discType) {
        synchronized(lock){
            lock.notify();
        }
        switch (discType) {
            case DiscoveryListener.INQUIRY_COMPLETED :
                System.out.println("INQUIRY_COMPLETED");
                break;
            case DiscoveryListener.INQUIRY_TERMINATED :
                System.out.println("INQUIRY_TERMINATED");
                break; case DiscoveryListener.INQUIRY_ERROR :
            System.out.println("INQUIRY_ERROR");
            break;
            default : System.out.println("Unknown Response Code");
                break;
        }
    }
}