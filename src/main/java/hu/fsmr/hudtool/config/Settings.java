package hu.fsmr.hudtool.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by blase on 2018.03.01..
 */
public class Settings {
    private Map<String, String> connections;
    private static Settings settings = null;


    public static Settings getInstance() {
        if (settings == null) {
            settings = loadSettings();
        }
        return settings;
    }

    private Settings() {
        connections = new HashMap<String, String>();
    }

    public void addConnection(String key, String value) {
        connections.put(key, value);
        saveSettings();
    }

    public void removeConnection(String key) {
        connections.remove(key);
        saveSettings();
    }

    private static Settings loadSettings() {
        ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
        Settings settings = null;

        try {
            settings = mapper.readValue(new File("settings.yaml"), Settings.class);
        } catch (Exception e) {
            settings = new Settings();
        }

        return settings;
    }

    public static void saveSettings() {
        ObjectMapper mapper = new ObjectMapper(new YAMLFactory());

        try {
            mapper.writeValue(new File("settings.yaml"), settings);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void resetSettings() {
        ObjectMapper mapper = new ObjectMapper(new YAMLFactory());

        try {
            mapper.writeValue(new File("settings.yaml"), new Settings());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Map<String, String> getConnections() {
        return connections;
    }

    public void setConnections(Map<String, String> connections) {
        this.connections = connections;
    }
}


