package hu.fsmr.hudtool.ui;

import hu.fsmr.hudtool.bluetooth.BluetoothDeviceDiscovery;
import hu.fsmr.hudtool.bluetooth.BluetoothSession;
import hu.fsmr.hudtool.config.Settings;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;

import javax.bluetooth.RemoteDevice;
import java.io.IOException;
import java.util.Map;

/**
 * Created by blase on 2018.02.28..
 */
public class SettingsController {

    private ObservableList<RemoteDevice> availableConnections;
    private ObservableList<RemoteDevice> savedConnections;

    private BluetoothSession bs;

    @FXML
    private ListView availableBtConnectionsListView;

    @FXML
    private ListView savedBtConnectionsListView;

    @FXML
    private Button scanButton;

    @FXML
    private Button addConnectionButton;

    @FXML
    private Button removeConnectionButton;

    @FXML
    private Button connectButton;

    @FXML
    private Button onButton;

    @FXML
    private Button offButton;

    @FXML
    private Button disconnectButton;


    @FXML
    private void initialize() {
        availableConnections = FXCollections.observableArrayList();
        savedConnections = FXCollections.observableArrayList();
        savedBtConnectionsListView.setItems(savedConnections);
        availableBtConnectionsListView.setItems(availableConnections);
        if (savedConnections.size() == 0) {
            removeConnectionButton.setDisable(true);
        }
        if(availableConnections.size() == 0) {
            addConnectionButton.setDisable(true);
        }
       /* Settings settings = Settings.getInstance();
        for (Map.Entry<String, String> stringStringEntry : settings.getConnections().entrySet()) {
            System.out.println(stringStringEntry);
        }*/
    }

    @FXML
    private void scanButtonHandler() {
        availableBtConnectionsListView.getItems().clear();
        availableBtConnectionsListView.setDisable(false);
        availableConnections = new BluetoothDeviceDiscovery().getDevices();

        if( null == availableConnections ){
            availableBtConnectionsListView.setItems(FXCollections.observableArrayList("Not found"));
            availableBtConnectionsListView.setDisable(true);
            return;
        }

        for (RemoteDevice availableConnection : availableConnections) {
            try {
                System.out.println(availableConnection.getBluetoothAddress() + " (" + availableConnection.getFriendlyName(true) + ")");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        availableBtConnectionsListView.setItems(availableConnections);

        if (availableConnections.size() > 0) {
            addConnectionButton.setDisable(false);
        }
    }

    @FXML
    private void addConnectionButtonHandler() {
        RemoteDevice rd = (RemoteDevice) availableBtConnectionsListView.getSelectionModel().getSelectedItem();
        savedConnections.add(rd);
        try {
            Settings.getInstance().addConnection(rd.getFriendlyName(true),rd.getBluetoothAddress());
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (availableConnections.size() == 0) {
            addConnectionButton.setDisable(true);
        }
        removeConnectionButton.setDisable(false);
    }

    @FXML
    private void removeConnectionButtonHandler() {
        savedConnections.remove(savedBtConnectionsListView.getSelectionModel().getSelectedItem());
    }

    @FXML
    private void connectButtonHandler(){
        RemoteDevice rd = (RemoteDevice) savedBtConnectionsListView.getSelectionModel().getSelectedItem();
        try {
            //bs = new BluetoothSession("btspp://"+rd.getBluetoothAddress()+":1;authenticate=false;encrypt=false;master=false");
            bs = new BluetoothSession("btspp://"+rd.getBluetoothAddress()+":1");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void onButtonHandler(){
        bs.sendMessage("1");
    }

    @FXML
    private void offButtonHandler(){
        bs.sendMessage("0");
    }

    @FXML
    private void disconnectButtonHandler(){
        bs.close();
    }
}
