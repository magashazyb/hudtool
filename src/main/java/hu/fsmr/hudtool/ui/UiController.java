package hu.fsmr.hudtool.ui;

import hu.fsmr.hudtool.utils.FileUtils;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Bounds;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Circle;
import javafx.stage.Modality;
import javafx.stage.Popup;
import javafx.stage.Stage;



import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;


/**
 * Created by blase on 2018.01.26..
 */
public class UiController {

    @FXML
    private Pane editPane;

    @FXML
    private Pane prevPane;

    @FXML
    private BorderPane rootLayout;

    @FXML
    private ImageView prevImage;

    @FXML
    private ComboBox scenesBox;



    private GridElement[][] grid;
    private FileUtils fileUtils;


    public UiController() {
        fileUtils = new FileUtils();
    }

    @FXML
    private void initialize() {
        defaultGrid();
        renderImage();
        loadScenes();
        editPane.setOnMousePressed(e -> renderImage());
    }

    private void loadScenes(){
        File[] drawScenes = fileUtils.getScenes();
        for (File drawScene : drawScenes) {
            scenesBox.getItems().add(drawScene.getName());
        }
        if( drawScenes.length != 0){
            scenesBox.getSelectionModel().selectFirst();
        }
    }



    @FXML
    private void newSceneButtonHandler(){
        Stage popupwindow=new Stage();
        popupwindow.setMaxHeight(90);
        popupwindow.initModality(Modality.APPLICATION_MODAL);
        popupwindow.setTitle("Add new scene");

        TextField newScene = new TextField();
        Button saveButton= new Button("Save");
        Button cancelButton= new Button("Cancel");

        cancelButton.setOnAction(e -> popupwindow.close());
        saveButton.setOnAction(e -> {
            createNewScene(newScene.getText());
            popupwindow.close();
        });


        VBox layout= new VBox(5);
        HBox buttons = new HBox();
        buttons.getChildren().addAll(saveButton, cancelButton);

        layout.getChildren().addAll(newScene, buttons);
        layout.setAlignment(Pos.CENTER);

        Scene popupScene= new Scene(layout, 300, 250);

        popupwindow.setScene(popupScene);
        popupwindow.showAndWait();
    }

    private void createNewScene(String sceneName){
        fileUtils.createScenesFolder(sceneName);
        scenesBox.getItems().add(sceneName);
    }

    @FXML
    private void buttonHandler() {
        System.out.println("CUSTOM BUTTON");
    }

    public void defaultGrid() {
        defaultGrid( new GridElement[64][48] );
    }

    public void defaultGrid(GridElement[][] gridElements){
        editPane.getChildren().clear();
        grid = gridElements;
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[0].length; j++) {
                grid[i][j] = new GridElement(i, j, 15);
                editPane.getChildren().add(grid[i][j]);
            }
        }
        rootLayout.setCenter(editPane);
    }

    @FXML
    private void save(){
        FileUtils.saveToFile(grid, "test1");
    }

    @FXML
    private void load(){
        grid = FileUtils.loadFromFile();
        renderImage();
        defaultGrid(grid);
    }

    @FXML
    private void renderImage() {
        int width = 64;
        int height = 48;
        BufferedImage img = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        Color c;

        for (int y = 0; y < grid[0].length; y++) {
            for (int x = 0; x < grid.length; x++) {

                if ( grid[x][y].isActive()){
                    c = new Color(255,255,255);
                }else{
                    c = new Color(0,0,0);
                }

                img.setRGB(x,y,c.getRGB());
            }
        }
        Image image = SwingFXUtils.toFXImage(img, null);
        prevImage.setImage(image);
    }
}
