package hu.fsmr.hudtool.ui;

import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

/**
 * Created by blase on 2018.01.26..
 */
public class GridElement extends StackPane {
    private int tileSize;
    private int x, y;
    private boolean isActive = false;
    private Rectangle border = null;


    public GridElement(int x, int y, int tileSize) {
        this(x, y, tileSize, true);
    }

    public GridElement(int x, int y, int tileSize, boolean isClickable) {
        this.x = x;
        this.y = y;

        this.tileSize = tileSize;

        border = new Rectangle(this.tileSize - 0.02, this.tileSize - 0.02);
        border.setStroke(Color.LIGHTGRAY);
        border.setFill(Color.GREY);

        getChildren().addAll(border);

        setTranslateX(x * this.tileSize);
        setTranslateY(y * this.tileSize);

        if (isClickable) {
            setOnMouseClicked(e -> drawIcon());
        }
    }

    private void drawIcon() {
        isActive = !isActive;
        if (isActive) {
            border.setFill(Color.WHITE);
        } else {
            border.setFill(Color.GREY);
        }
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }
}
