package hu.fsmr.hudtool;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

import java.io.IOException;


/**
 * Created by blase on 2018.01.26..
 */
public class AppStart extends Application {
    private Stage primaryStage;
    private BorderPane rootLayout;
    private BorderPane tabPane;


    @Override
    public void start(Stage primaryStage) throws Exception {
        this.primaryStage = primaryStage;
        this.primaryStage.setTitle("HUD tool");
        this.primaryStage.setResizable(false);
        this.primaryStage.setMaximized(true);

        initRootLayout();
    }

    public static void main(String[] args) {
        launch(args);
    }

    private void initRootLayout() {
        try {
            rootLayout = (BorderPane) FXMLLoader.load(getClass().getClassLoader().getResource("views/RootLayout.fxml"));
            Scene scene = new Scene(rootLayout);
            primaryStage.setScene(scene);
            primaryStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    /*
        --->64
        |
        |
        |
       \/48
     */
    //64*48
}